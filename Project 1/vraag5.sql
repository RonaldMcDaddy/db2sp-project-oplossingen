create or replace FUNCTION BEPAAL_VOORVEREISTE_CURSUS2(
    p_cursusnummer IN courses.course_no%TYPE
) RETURN VARCHAR2 IS
    v_description   courses.description%TYPE;
    v_pre_id        courses.prerequisite%TYPE;
    v_pre_desc      courses.description%TYPE;
BEGIN
    SELECT description, prerequisite INTO v_description, v_pre_id
    FROM courses
    WHERE course_no = p_cursusnummer;
    
    IF v_pre_id IS NULL THEN
        RETURN 'geen';
    ELSE
        SELECT description INTO v_pre_desc
        FROM courses
        WHERE course_no = v_pre_id;
        
        RETURN v_pre_desc;
    END IF;
END BEPAAL_VOORVEREISTE_CURSUS2;
