CREATE OR REPLACE PROCEDURE VOEG_GRADE_TOE(
    p_student_id            IN grades.student_id%TYPE,
    p_section_id            IN grades.section_id%TYPE,
    p_grade_type_code       IN grades.grade_type_code%TYPE,
    p_grade_code_occurrence IN grades.grade_code_occurrence%TYPE,
    p_numeric_grade         IN grades.numeric_grade%TYPE,
    p_comments              IN grades.comments%TYPE
) IS
BEGIN
    INSERT INTO grades
    VALUES(p_student_id, p_section_id, p_grade_type_code, p_grade_code_occurrence, p_numeric_grade, p_comments,
        USER, SYSDATE, USER, SYSDATE);
    
    COMMIT;
    
    DBMS_OUTPUT.PUT_LINE('Grade voor student ' || p_student_id || ' in de sectie ' ||
        p_section_id || ' succesvol toegevoegd.');
END VOEG_GRADE_TOE;
