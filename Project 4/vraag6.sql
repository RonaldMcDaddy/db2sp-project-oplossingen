create or replace PROCEDURE AANVULLEN_ENROLLMENTS IS 
    TYPE type_extra_to_enrol
    IS TABLE OF enrollments%ROWTYPE;
    t_enroll type_extra_to_enrol;

    e_bulk_errors EXCEPTION;
    PRAGMA EXCEPTION_INIT(e_bulk_errors, -24381);
BEGIN
    SELECT * BULK COLLECT INTO t_enroll
    FROM enrollments_extra; --je zet alles in een collectie om die later op te roepen
    
    FORALL i IN t_enroll.FIRST..t_enroll.LAST --je gaat elke rij inserten 
                                              --FORALL om impliciete satatement te kunnen toevoege SQL%bulk_exceptions.count
    SAVE EXCEPTIONS /*collection elementen die geen runtime error opleveren wel worden 
                    geïnserteerd en dat er een foutmelding komt voor de rijen die wel problemen opleveren.*/
    
    INSERT INTO enrollments
    VALUES t_enroll(i);

    EXCEPTION
        WHEN e_bulk_errors THEN
            FOR i IN 1..SQL%BULK_EXCEPTIONS.COUNT LOOP
                DBMS_OUTPUT.PUT_LINE(SQL%BULK_EXCEPTIONS (i).ERROR_INDEX);
                DBMS_OUTPUT.PUT_LINE(SQLERRM(-1 * SQL%BULK_EXCEPTIONS(i).ERROR_CODE));
            END LOOP;
    COMMIT;
END AANVULLEN_ENROLLMENTS;
