create or replace PROCEDURE VOEG_INSCHRIJVING_TOE (
    p_student_id    IN enrollments.student_id%TYPE,
    p_section_id    IN enrollments.section_id%TYPE
) IS
    v_final_grade NUMBER DEFAULT 0;

    e_pk_student_section EXCEPTION;  
    PRAGMA EXCEPTION_INIT(e_pk_student_section,-2291);
BEGIN
    INSERT INTO enrollments
    VALUES(p_student_id, p_section_id, SYSDATE, v_final_grade, USER, SYSDATE, USER, SYSDATE);
    
    EXCEPTION
        WHEN e_pk_student_section THEN          
            DBMS_OUTPUT.PUT_LINE('Student en/of sectie werd niet gevonden!');

    COMMIT;
END VOEG_INSCHRIJVING_TOE;
