create or replace PROCEDURE AVG_GRADE_CURSOR (
    p_section_id        sections.section_id%TYPE,
    p_grade_type_code   grades.grade_type_code%TYPE
) IS 
    CURSOR cur_avg_grades IS 
        SELECT s.section_id, c.description, g.student_id, AVG(g.numeric_grade) gemid
        FROM grades g
        JOIN enrollments e ON (g.section_id = e.section_id)
        JOIN sections s ON (e.section_id = s.section_id)
        JOIN courses c ON (s.course_no = c.course_no)
        WHERE s.section_id=p_section_id AND g.grade_type_code=p_grade_type_code
        GROUP BY s.section_id, c.description, g.student_id;
BEGIN
    FOR r_avg_grade IN cur_avg_grades LOOP
        DBMS_OUTPUT.PUT_LINE(r_avg_grade.section_id || ' ' || r_avg_grade.description || ' ' || r_avg_grade.student_id || ' ' || r_avg_grade.gemid);
    END LOOP;
END AVG_GRADE_CURSOR;
