create or replace PROCEDURE NOG_PLAATS_SECTIONS_CUR IS
    v_aantal_inschr NUMBER;
    
    CURSOR cur_nog_plaats IS
        SELECT s.section_id, c.description, s.capacity
        FROM sections s
        JOIN courses c ON s.course_no=c.course_no
        WHERE LOWER(c.description) LIKE '%intro%';
BEGIN
    FOR r_nog_plaats IN cur_nog_plaats LOOP
        v_aantal_inschr := aantal_inschrijvingen(r_nog_plaats.section_id);
        
        IF r_nog_plaats.capacity > (v_aantal_inschr + 10) THEN
            DBMS_OUTPUT.PUT_LINE(r_nog_plaats.section_id || ' ' || r_nog_plaats.description);
        END IF;
    END LOOP;
END NOG_PLAATS_SECTIONS_CUR;


-- aantal_inschrijvingen


create or replace FUNCTION AANTAL_INSCHRIJVINGEN(
    p_section_id IN sections.section_id%TYPE
) RETURN NUMBER IS
    v_aantal NUMBER;
BEGIN
    SELECT COUNT(*) INTO v_aantal
    FROM enrollments
    WHERE p_section_id=section_id;

    RETURN v_aantal;
END AANTAL_INSCHRIJVINGEN;
