# Databanken 2: Stored Procedures Project oplossing

In deze repository staan een groot deel van de oplossingen uit de projecten van het vak Databanken 2: Stored Procedures (2018-2019)

## Toevoegen/aanpassen

Als je een oplossing wilt toevoegen of aanpassen, ga ervoor. Ik zal de aanpassingen zo snel mogelijk bekijken.