create or replace TRIGGER TR_WEIGER_INSCHRIJVING 
BEFORE INSERT ON enrollments
FOR EACH ROW
DECLARE
    v_aantal NUMBER := SA_PKG.AANTAL_INSCHRIJVINGEN(:NEW.section_id);
    v_capacity sections.capacity%type;
BEGIN
        SELECT capacity INTO v_capacity
        FROM sections
        WHERE :NEW.section_id = section_id;

        IF v_aantal >= v_capacity THEN
             RAISE_APPLICATION_ERROR(-20001, 'GEWEIGERD!');
        END IF;
END;
