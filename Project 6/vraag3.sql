create or replace TRIGGER TR_AUDIT_PUNTEN 
BEFORE INSERT OR UPDATE ON GRADES 
BEGIN
    IF TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) < 8
    OR TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) > 18
    OR TO_NUMBER(TO_CHAR(SYSDATE, 'D')) > 5 THEN
        RAISE_APPLICATION_ERROR(-20000, 'Punten kunnen alleen worden aangepast op weekdagen tijdens de kantooruren.');
    END IF;
END;
