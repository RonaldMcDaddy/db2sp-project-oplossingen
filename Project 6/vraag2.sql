create or replace TRIGGER TR_AUDIT_COURSE
BEFORE UPDATE OR INSERT ON courses
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        :NEW.created_by := USER;
        :NEW.created_date := SYSDATE;
    END IF;
    
    :NEW.modified_by := USER;
    :NEW.modified_date := SYSDATE;
END;
