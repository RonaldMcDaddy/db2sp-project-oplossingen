create or replace TRIGGER TR_AUDIT_COURSE_DATUM
BEFORE INSERT OR UPDATE ON ENROLLMENTS
FOR EACH ROW
DECLARE
    v_start_datum DATE;
BEGIN
    SELECT start_date_time INTO v_start_datum
    FROM sections
    WHERE section_id = :NEW.section_id;

    IF :NEW.enroll_date > v_start_datum THEN
        RAISE_APPLICATION_ERROR(-20000, 'Deze sectie is al begonnen!');
    END IF;
END;
