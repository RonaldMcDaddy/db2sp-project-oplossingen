create or replace TRIGGER TR_AUDIT_ENROLL_JAAR 
BEFORE INSERT ON ENROLLMENTS
FOR EACH ROW
DECLARE
    v_jaar_inschrijving     VARCHAR2(4);
    v_aantal_inschrijvingen NUMBER;
BEGIN
    v_jaar_inschrijving := TO_CHAR(:NEW.enroll_date, 'yyyy');
    
    SELECT COUNT(*) INTO v_aantal_inschrijvingen
    FROM enrollments
    WHERE TO_CHAR(enroll_date, 'yyyy') = v_jaar_inschrijving AND student_id = :NEW.student_id;
    
    IF v_aantal_inschrijvingen >= 4 THEN
        RAISE_APPLICATION_ERROR(-20000, 'Student ' || :NEW.student_id || ' heeft zich al voor het maximale aantal (4) secties ingeschreven!');
    END IF;
END;
